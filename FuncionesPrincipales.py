import multiprocessing
from builtins import print
from math import ceil
from pyscipopt import *
import os
import time

cotaMaximaCantRemedios = 0
resultadosValores = ""
valorObjetivo=0
sumaRequeridas=0
cantidadRemedios=0

def optimizar():
    print("---------------------------------")
    print("Input: ")
    print("Costo Envasado: K=" + str(costoEnvasado))
    print("Drogas disponibles: D=" + str(drogasDisponibles))
    print("Factor Inestabilidad Maximo: H=" + str(factorMax))
    print("---------------------------------")
    print("Variables: ")
    print("p_{rd}: cantidad que posee el remedio r de la droga d (con r e R y d e D). p_{rd}=1 si el remedio r posee una unidad de la droga d, p_{rd}=0 si no")
    print("x_r: decision de fabricar el remedio r con x_r=1 si se fabricara el remedio r o x_r=0 si no")
    print("---------------------------------")
    global resultadosValores
    global valorObjetivo
    global cantidadRemedios
    # --------------------
    # Creo el modelo
    model = Model("CoctelDrogas")

    # --------------------
    # Agrego variables al modelo
    p, x = {}, {}  # p: lista con las variables: de la cantidad de droga de cada remedio, x: decisiones de fabricar el remedio r
    numeracionRemedios = []  # me creo un array con los indices de todos los remedios disponibles
    numeracionDrogas = []  # me creo un array con los indices de todos las drogas disponibles
    numDroga = 0
    d = 0
    print("Empiezo a crear variables")
    for droga in drogasDisponibles:
        # creo un array con los numeros de todas las drogas
        numeracionDrogas.append(numDroga)
        numDroga += 1
    # creo variables segun la cota hallada
    for i in range(1,cotaMaximaCantRemedios+1):
        numeracionRemedios.append(i - 1) #voy guardando los numeros de remedios en un array
        nombreVariable = "x" + str(i)  # armo el nombre de las variables
        x[i - 1] = model.addVar(nombreVariable,
                                vtype="BINARY")  # agrego la variable x_r al modelo y la declaro como binaria
        for droga in drogasDisponibles:
            nombreVariable = "p" + str(i) + str(droga)
            p[i - 1, d] = model.addVar(nombreVariable,
                                       vtype="BINARY")  # agrego la variable p_{rd} al modelo y la declaro como binaria
            d += 1
        i += 1  # aumento el numero del contador
        d = 0
    # --------------------costoActual=drogasDisponibles["d" + str(d + 1)]["costo unitario"]
    # defino funcion objetivo
    print("Defino funcion objetivo")
    model.setObjective(quicksum(costoEnvasado * x[r] + quicksum((drogasDisponibles["d" + str(d + 1)]["costo unitario"]) * p[r, d] for d in numeracionDrogas) for r in numeracionRemedios), "minimize")

    # --------------------
    # defino restricciones
    # ---------------------------------
    # restriccion para cumplir la cantidad de droga solicitada para el paciente
    print("agrego restricciones")
    for d in numeracionDrogas:
        nombreKey = "d" + str(d + 1)
        model.addCons(
            quicksum(p[r, d] for r in numeracionRemedios) == drogasDisponibles[nombreKey]["unidades requeridas"])

    cantidadRemedios=numeracionRemedios.__len__()
    print("Cantidad de remedios utilizados en el modelo: ", cantidadRemedios)
    # restriccion para validar que cada remedio no supere el factor maximo de inestabilidad teniendo en cuenta
    # el factor de cada droga que lo compone
    for r in numeracionRemedios:
        model.addCons(quicksum(drogasDisponibles["d"+str(d+1)]["factor de inestabilidad"] * p[r, d] for d in numeracionDrogas) <= factorMax)
    # restriccion para relacionar las variables x_r y p_{rd}
    for r in numeracionRemedios:
        for d in numeracionDrogas:
            model.addCons(p[r, d] <= x[r])

    # -----------------------------------------------
    model.hideOutput(True)
    # optimizo el modelo
    print("Voy a optimizar")
    model.optimize()
    # obtengo solucion
    solucion = model.getBestSol()
    # si hay solucion óptima, la imprimo por pantalla sino muestro una advertencia
    if solucion == "{}":
        resultadosValores = "SOLUCIÓN VACÍA"
        print("SOLUCIÓN VACÍA")
    elif (model.getStatus() != "infeasible"):
        print("Se encontró un resultado optimo factible. Ver archivo generado para más información.")
        valorObjetivo=model.getObjVal()
        resultadosValores = str(solucion)
    else:
        resultadosValores = "NO SE ENCONTRARON SOLUCIONES FACTIBLES"
        print("NO SE ENCONTRARON SOLUCIONES FACTIBLES")


def Coctel(farmacia, remedio):
    # creo una variable del momento en que inicia el metodo
    inicioTiempo = time.time()
    global remedioNuevo
    # Uso el mismo metodo para cargar tanto la droga como el remedio
    leerTxtFarmacia(farmacia)  # lectura del primer archivo ("drogas")
    leerTxtFarmacia(remedio)  # lectura del primer archivo ("remedios")
    extraerFactorInestabilidadMax() #extraigo el factor de inestabilidad maximo
    extraerCostoEnvasado()#extraigo el costo de envasado
    acotarDrogas() #acoto las drogas
    print("La cota maxima hallada es de a lo sumo:",cotaMaximaCantRemedios,"remedios")

    optimizar()
    finTiempo = time.time() #creo una variable del momento que termine de ejecutar todo
    global tiempoEjecucion #creo una global para guardar el tiempo de ejecucion
    tiempoEjecucion = finTiempo - inicioTiempo
    generarArchivoResultados() #genero resultados


def generarArchivoResultados():
    # llamo a las globales
    global resultadosValores
    global sumaRequeridas
    global valorObjetivo
    global cantidadRemedios
    print("Tiempo de ejecución en segundos: ", tiempoEjecucion)
    pathArchivo = os.getcwd() + "/Resultados.txt"
    contador = 0
    # valido que no exista un archivo con ese nombre en el directorio, sino voy agregandole un numero
    while (os.path.exists(pathArchivo)):
        contador += 1
        pathArchivo = os.getcwd() + "/" + "Resultados" + str(contador) + ".txt"
    # abro el archivo
    file = open(pathArchivo, "w")
    # escribo los resultados hallados
    file.write("Cantidad de remedios utilizados en el modelo: " + str(cantidadRemedios)+ os.linesep)
    file.write("Suma de requeridas: " + str(sumaRequeridas) + os.linesep)
    file.write("Valor Objetivo: " + str(valorObjetivo) + os.linesep)
    file.write("Tiempo de Ejecución: " + str(tiempoEjecucion) + " segundos" + os.linesep)
    file.write("Resultados obtenidos: " + os.linesep)
    file.write(resultadosValores + os.linesep)
    # cierro el archivo
    file.close()
    print("Archivo de resultados: "+pathArchivo)

def extraerFactorInestabilidadMax():
    global factorMax
    factorMax = 0
    for r in remediosConfiguracion:
        for config in remediosConfiguracion[r]:
            if (config == "Factor de inestabilidad maxima"):
                factorMax = remediosConfiguracion[r][config]


def extraerCostoEnvasado():
    global costoEnvasado
    costoEnvasado = 0
    for r in remediosConfiguracion:
        for config in remediosConfiguracion[r]:
            if (config == "costo del envase"):
                costoEnvasado = remediosConfiguracion[r][config]

def acotarDrogas():
    global cotaMaximaCantRemedios
    global sumaRequeridas
    valorG=0 #inicializo el valor de G
    if (drogasDisponibles.__len__() == 0): #controlo que no me llegue un array de drogas vacio
        print("No hay drogas que NO superen el factor de inestabilidad maximo. Es imposible fabricar remedios asi.")
    else:
        superacionFactor = False
        factoresInestabilidadDrogas = [] #creo un array con los factores de inestabilidad de las drogas
        for droga in drogasDisponibles:
            factoresInestabilidadDrogas.append(drogasDisponibles[droga]["factor de inestabilidad"]) #guardo todos los factores de inestabilidad en el array
            sumaRequeridas += drogasDisponibles[droga]["unidades requeridas"] #calculo el valor de las drogas requeridas totales
        print("Factor maximo: ",factorMax)
        print("Suma Drogas Requeridas: ",sumaRequeridas)
        # busco el valor de k con el que luego busco la cota de remedios
        acumuladorSuma=0 #creo un acumulador para ir validando que no supere el maximo
        while superacionFactor == False: #itero hasta que supere el factor maximo
            if (factoresInestabilidadDrogas.__len__() == 0 or acumuladorSuma >= factorMax):
                superacionFactor = True
                if(acumuladorSuma > factorMax):
                    valorG -= 1  # ajusto ya que en la vuelta anterior sume otro k cuando no es asi
                break
            drogaMaxFactor = max(factoresInestabilidadDrogas) #busco el maximo del array
            acumuladorSuma = acumuladorSuma + drogaMaxFactor #sumo el maximo hallado al acumulador
            factoresInestabilidadDrogas.remove(drogaMaxFactor)# saco del array el maximo hallado
            valorG += 1#aumento el valor de g
        print("Valor de G: ",valorG)
        cotaMaximaCantRemedios=ceil(sumaRequeridas/valorG) #realizo la division y obtengo la cota. Redondeo para arriba


def leerDatos():
    # Lee el input del usuario y lo manda a la funcion principal Coctel para que haga el resto del trabajo
    # creo variables globales a usar luego
    global drogasDisponibles
    global remediosConfiguracion
    global cotaMaximaCantRemedios
    global minimoDrogasPorRemedio
    global listaRemedios
    drogasDisponibles = {}
    remediosConfiguracion = {}
    cotaMaximaCantRemedios = 0
    listaRemedios = []
    # solicito ele ingreso de los inputs
    rutaFarmaciaTxt = input("Ingrese la ruta del archivo de texto (Drogas):")
    rutaRemedioNuevoTxt = input("Ingrese la ruta del archivo de texto (Remedios):")

    # creo una variable con los 10 minutos solicitados para este TP
    minutos = 60 * 10
    # seteo el metodo que sera temporizado por 10 minutos. En este caso Coctel()
    p = multiprocessing.Process(target=Coctel, name="Coctel", args=(rutaFarmaciaTxt, rutaRemedioNuevoTxt,))
    # doy inicio al temporizador
    p.start()
    # seteo que sea por 10 minutos
    p.join(minutos)
    # si al pasar 10 minutos el proceso sigue vivo lo termino y muestro un print por pantalla
    if p.is_alive():
        p.terminate()
        p.join
        print("Han pasado 10 minutos de ejecucion. Se corto la misma.")


# Métodos encargados de la lectura de archivos

def generarParametros(z, t):
    VarOrigen = z
    count = 0
    params = ""
    cv = {}
    cvinterna = {}

    params = ''.join(VarOrigen.replace(": ", ":"))
    params = ''.join(VarOrigen.replace("\n", ""))

    if t == 'd':
        clavep, valorp = params.split(":")
        lista = valorp.split(",")
        Claves = ['costo unitario', 'unidades requeridas', 'factor de inestabilidad']

        for s in lista:
            params2 = ''.join(s)
            #            clave2, valor2 = params2.split(',')
            cvinterna[Claves[count]] = int(params2) if int(float(params2)) == float(params2) else float(params2)
            count = count + 1

        cv[clavep.strip()] = cvinterna
    else:
        lista = params.split(",")
        Claves = ['costo del envase', 'Factor de inestabilidad maxima']

        for s in lista:
            params2 = ''.join(s)
            #            clave2, valor2 = params2.split(',')
            cvinterna[Claves[count]] = int(params2) if int(float(params2)) == float(params2) else float(params2)
            count = count + 1

        cv[NombreFileEntrada] = cvinterna

    return cv


def leerTxtRemedioNuevo(ruta_txt):
    print("Leyendo: " + ruta_txt)
    try:
        archivo = open(ruta_txt, 'r')
        patharchivo = os.path.splitext(ruta_txt)
        contenido = archivo.readlines()
        global NombreFileEntrada
        NombreFileEntrada = patharchivo[0].split("/").pop()
        tipoParametro = ''

        if contenido == '':  # validar si el archivo esta vacio
            print("El archivo seleccionado esta vacio por favor verifique su texto de entrada.")
        else:
            global remedioNuevo
            remedioNuevo = {}
            cont = 0

            for f in contenido:
                cont = 0
                if not f.__contains__("#") and f != '\n':
                    if f.__contains__("drogas") or f.__contains__("DROGAS"):
                        tipoParametro = 'd'
                        cont = 1
                    elif f.__contains__("remedios") or f.__contains__("REMEDIOS"):
                        tipoParametro = 'r'
                        cont = 1

                    if cont == 0:
                        if tipoParametro == 'r':
                            remedioNuevo.update(generarParametros(f, tipoParametro))
                            break


    except ValueError:
        print("Error no controlado." + ValueError)

    finally:
        archivo.close()


def leerTxtFarmacia(ruta_txt):
    print("Leyendo: " + ruta_txt)
    try:
        archivo = open(ruta_txt, 'r')
        patharchivo = os.path.splitext(ruta_txt)
        contenido = archivo.readlines()
        global NombreFileEntrada
        NombreFileEntrada = patharchivo[0].split("/").pop()
        tipoParametro = ''

        if contenido == '':  # validar si el archivo esta vacio
            print("El archivo seleccionado esta vacio por favor verifique su texto de entrada.")
        else:

            cont = 0

            if NombreFileEntrada.__contains__("droga") or NombreFileEntrada.__contains__("DROGA"):
                tipoParametro = 'd'
                cont = 1
            elif NombreFileEntrada.__contains__("remedio") or NombreFileEntrada.__contains__("REMEDIO"):
                tipoParametro = 'r'
                cont = 1

            for f in contenido:
                cont = 0
                if not f.__contains__("#") and f != '\n':

                    if cont == 0:
                        if tipoParametro == 'd':
                            drogasDisponibles.update(generarParametros(f, tipoParametro))
                        else:
                            remediosConfiguracion.update(generarParametros(f, tipoParametro))



    except ValueError:
        print("Error no controlado." + ValueError)

    finally:
        archivo.close()
