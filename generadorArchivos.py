import os
import random

global f
global r
global pathDirectorio

def crearDirectorioInputs():
    global pathDirectorio
    # uso os.getswd() para obtener el path actual
    pathDirectorio = os.getcwd() + "/inputs"
    contador = 1
    # valido si existe un path con ese nombre, sino agrego un numero +1 hasta que no exista
    while (os.path.exists(pathDirectorio)):
        pathDirectorio = os.getcwd() + "/inputs" + str(contador)
        contador += 1
    # creo el directorio
    os.mkdir(pathDirectorio)

def generarArchivoRemediosIn(rInput, costoEnvase):
    r = rInput
    print("r utilizado: ",r)
    print("Voy a generar remedios.in")
    # busco un valor random para el factor de inestabilidad maximo
    factorInestabilidadMax=random.randint(30,35)
    global f
    f=factorInestabilidadMax
    nombreArchivo="remedios"
    # armo el path donde se creara
    pathArchivo=pathDirectorio+"/"+nombreArchivo+".in"
    contador=0
    # valido que no exista un archivo con ese nombre, sino agrego un numero hasta que no exista
    while(os.path.exists(pathArchivo)):
       contador+=1
       pathArchivo=pathDirectorio+"/"+nombreArchivo+str(contador)+".in"
    # abro el archivo
    file = open(pathArchivo, "w+")
    # escribo la informacion que necesito
    file.write("# costo del envase, Factor de inestabilidad maxima" + os.linesep)
    file.write(costoEnvase + ", " + str(factorInestabilidadMax))
    # cierro el archivo
    file.close()
    print("Archivo generado en: " + pathArchivo)
def generarArchivoDrogasIn(rInput):
    global pathDirectorio
    # creo una variable para controlar la cantidad de ejecuciones. En este caso, cambia a true cuando ya cree 3 archivos
    realiceTresEjecuciones=False
    restaR=2
    while(not realiceTresEjecuciones):
        print("Voy a generar drogas.in")
        # uso r para saber cuantas lineas llevara el archivo drogas.in
        r = int(rInput)-restaR
        print(r)
        nombreArchivo = "drogas"
        pathArchivo = pathDirectorio + "/" + nombreArchivo + ".in"
        contador = 0
        # guardo en esta variable cuantas drogas debo generar
        drogasGenerar = pow(2, int(r) + 1)
        print("drogas a generar: ", drogasGenerar)
        # valido si el archivo existe, sino subo el contador para agregar ese numero al nombre del archivo
        while (os.path.exists(pathArchivo)):
            contador += 1
            pathArchivo = pathDirectorio + "/" + nombreArchivo + str(contador) + ".in"

        # abro el archivo
        file = open(pathArchivo, "w+")
        # empiezo a escribir el archivo
        file.write("# Para c/droga: costo unitario,  unidades requeridas, factor de inestabilidad" + os.linesep)
        contador = 0
        # ciclo para iterar sobre la cantidad de drogas a generar
        while (contador < drogasGenerar):
            contador += 1
            # busco un costo random
            costoRandom = random.randint(1, 25)
            # busco las unidades requeridas de la forma que lo pide el enunciado
            unidadesRequeridas = random.randint(pow(2, int(r)), pow(2, int(r) + 1))
            # busco el factor de inestabilidad como lo pide el enunciado, donde f es el factor de inestabilidad max
            factorInestabilidad = random.randint(int(f / 7), int(f / 4))
            # con los datos obtenidos antes, empiezo a escribir el archivo
            # distingo en dos escritura ya que en la ultima no agrego el salto de linea
            if (contador == drogasGenerar):
                file.write("d" + str(contador) + ": " + str(costoRandom) + ", " + str(unidadesRequeridas) + ", " + str(
                    factorInestabilidad))
            else:
                file.write("d" + str(contador) + ": " + str(costoRandom) + ", " + str(unidadesRequeridas) + ", " + str(
                    factorInestabilidad) + os.linesep)

        # cierro el archivo
        file.close()
        print("Archivo generado en: " + pathArchivo)
        if(restaR>0):
            restaR-=1
        else:
            realiceTresEjecuciones=True



if __name__ == '__main__':
    rInput=input("Ingrese el r deseado: ")
    costoEnvase=input("Ingrese el costo del envase (en enteros): ")
    # creo el directorio donde guardare los inputs
    crearDirectorioInputs()
    # genero el archivo de remedios
    generarArchivoRemediosIn(rInput, costoEnvase)
    # genero el archivo de drogas
    generarArchivoDrogasIn(rInput)
